﻿namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num = int.Parse(textBox1.Text);
            int n = int.Parse(textBox2.Text);

            int dec = 0;
            int power = 0;
            while (num > 0)
            {
                int remainder = num % 10;
                dec += remainder * (int)Math.Pow(n, power);
                power++;
                num /= 10;
            }

            int num2 = int.Parse(textBox3.Text);
            int n2 = int.Parse(textBox4.Text);
            int dec2 = 0;
            int power2 = 0;
            while (num2 > 0)
            {
                int remainder = num2 % 10;
                dec2 += remainder * (int)Math.Pow(n2, power2);
                power2++;
                num2 /= 10;
            }

            if (dec >= dec2)
            {
                label5.Text = $"Число{textBox1.Text} є більшим ніж число{textBox2.Text} ";
            }
            else
            {
                label5.Text = $"Число{textBox3.Text} є більшим ніж число{textBox4.Text}  ";
            }
        }
    }
}